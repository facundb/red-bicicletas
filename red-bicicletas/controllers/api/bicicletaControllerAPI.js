var Bicicleta = require('../../models/modelobicicleta')

exports.bicicleta_list = function(req, res){

    res.status(200).json({
        bicicletas:Bicicleta.allBicis
    });
}

exports.bicicleta_create = function (req, res) {
  let bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng]
  });
  
  Bicicleta.add(bici)

  res.status(200).json({
    bicicleta: bici
  })
}
